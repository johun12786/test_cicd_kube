from django.contrib import admin
from django.urls import path

# from django.conf.urls import include, re_path
from petdy import views

urlpatterns = [
    # url("admin/", admin.site.urls),
    path("petdy/webhook", views.Hook),
]
