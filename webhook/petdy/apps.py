from django.apps import AppConfig


class PetdyConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'petdy'
