from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from rest_framework import generics
from rest_framework.decorators import api_view
import json
import requests
from rest_framework import status
from rest_framework.response import Response


@api_view(["POST"])
def Hook(request):
    headers = {
        "Authorization": "Bearer A1f52b98be0f25416a6a9a262d15747cbfa622f189173425aa8b8ba03bf8d67822a6ab46d22c34e21835d0ec2bb50240d"
    }
    quick_reply_url = "https://chat-api.one.th/message/api/v1/push_quickreply"
    send_message_url = "https://chat-api.one.th/message/api/v1/push_message"
    data = request.data
    # print(data)
    # print(data["source"]["user_id"])
    # print(data["bot_id"])
    if data["message"]["text"] == "ติดต่อเจ้าหน้าที่":
        payload = {
            "to": data["source"]["user_id"],
            "bot_id": data["bot_id"],
            "message": "โปรดเลือกช่องทางในการติดต่อ",
            "quick_reply": [
                {
                    "label": "Line official",
                    "type": "link",
                    "url": "https://liff.line.me/1645278921-kWRPP32q/?accountId=petdy",
                    "sign": "false",
                    "onechat_token": "false",
                },
                {
                    "label": "Facebook",
                    "type": "link",
                    "url": "https://www.facebook.com/Petdy",
                    "sign": "false",
                    "onechat_token": "false",
                },
                {"label": "Email", "type": "text", "message": "Email", "payload": "email"},
                {"label": "Call center", "type": "text", "message": "Call center", "payload": "call center"},
            ],
        }
        r = requests.post(quick_reply_url, json=payload, headers=headers)
    if "data" in data["message"]:
        if data["message"]["data"] == "email":
            payload = {
                "to": data["source"]["user_id"],
                "bot_id": data["bot_id"],
                "type": "text",
                "message": "Email : info@pacheti.com",
                "custom_notification": "เปิดอ่านข้อความใหม่จากทางเรา",
            }
        elif data["message"]["data"] == "call center":
            payload = {
                "to": data["source"]["user_id"],
                "bot_id": data["bot_id"],
                "type": "text",
                "message": "Tel : 02 257 7199",
                "custom_notification": "เปิดอ่านข้อความใหม่จากทางเรา",
            }
        r = requests.post(send_message_url, json=payload, headers=headers)
    return Response(status=status.HTTP_200_OK)
